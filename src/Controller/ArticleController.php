<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

use App\Service\ArticleService;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use App\Service\MarkdownHelper;
use App\Service\SlackClient;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class ArticleController extends AbstractController
{
    /**
     * Currently unused: just showing a controller with a constructor!
     */
    private $isDebug;

    public function __construct(bool $isDebug)
    {
        $this->isDebug = $isDebug;
    }

    /**
     * @Route("/", name="article_list")
     */
    public function list(ArticleService $articleServices)
    {
       $articles =  $articleServices->listAll();

        $data = [];
            Foreach ($articles as $article) {
            $data[] = $article->toArray();  
            }
        
        return $this->json( [ 'articles' => $articles]);
    }

    /**
     * @Route("/new", name="news_article")
     */
    public function new( Request $request, EntityManagerInterface $entityManager){
        $success = true;
        $errors  = [];
        $status  = 200; 
            try {
                $request = Request::createFromGlobals();
                $params = array();
                $content = $request->getContent();
                if (empty($content)) {
                    throw new \Exception('Data was not received.', 400);
                }
                $params = json_decode($content, true); // 2nd param to get as array
                $entityManager = $this->getDoctrine()->getManager();
                $article = new Article();
                $article->setTitle($params['title']);
                $article->setSlug('http://localhost:8000/news');
                $article->setContent($params['content']);
                $article->setAuthor($params['author']);
                $article->setImageFilename('lightspeed.png');
          
                $entityManager->persist($article);
                $entityManager->flush();
          
            } catch (\Exception $ex) {
                $success = false;
                $errors[] = $ex->getMessage();
            }
             
            return $this->json(['success'=> $success,
                                'errors' => $errors,
                                'data'   => $success ? $article->toArray() : [],
                               ],$status); 
    }
             
    /**
     * @Route("/show/{id}", name="news_show")
     */
    public function getById(ArticleRepository $repository, $id){
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        if ($article) {
            $succes = true;
        } else {
            $succes = false;
        }
    
        return $this->json(['succes' => $succes,
                            'errors' => [],
                            'data'   => $article->toArray()
                           ]); 
    }

    /**
     * @Route("/edit/{id}", name="edit_article")
     */
     public function update(EntityManagerInterface $entityManager,$id){
        $success = true;
        $errors  = [];
        $status  = 200; 
            try {
                $request = Request::createFromGlobals();
                $params = array();
                $content = $request->getContent();
                if (empty($content)) {
                    throw new \Exception('Data was not received.', 400);
                }
          
                $params = json_decode($content, true); // 2nd param to get as array
                $entityManager = $this->getDoctrine()->getManager();
                $article = new Article();
                $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
                $article->setTitle($params['title']);
                $article->setSlug('http://localhost:8000/news');
                $article->setContent($params['content']);
                $article->setAuthor($params['author']);
                $article->setImageFilename('lightspeed.png');
          
                $entityManager->persist($article);
                $entityManager->flush();
          
            } catch (\Exception $ex) {
                $success = false;
                $errors[] = $ex->getMessage();
            }
             
        return $this->json(['success'=> $success,
                            'errors' => $errors,
                            'data'=> $success ? $article->toArray() : [],
                           ],$status); 
    }
              
   /**
    * @Route("/delete/{id}",name="article_delete")
    */
    public function delete(Request $request, $id){
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($article);
        $entityManager->flush();

        $response = new Response();
        return $this->redirectToRoute('article_list');
    }      
} 



