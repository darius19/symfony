<?php

namespace App\Service;

    use Symfony\Component\Cache\Adapter\AdapterInterface;
    use Symfony\Bundle\FrameworkBundle\Tests\Fixtures\Validation;
    use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
    use App\Repository\ArticleRepository;

    use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


use App\Service\ArticleService;

use App\Entity\Article;
use App\Repository\CommentRepository;
use App\Service\MarkdownHelper;
use App\Service\SlackClient;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;



    Class ArticleService{
        
        public function listAll(ArticleRepository $repository){
            $repository = $this->getArticleRepository();
            $articles = $repository->findAllPublishedOrderedByNewest();
            return $articles;
        }
    }